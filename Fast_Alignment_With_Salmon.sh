#!usr/bin/bash

# FAST TRIMMING, ALIGNMENTS AND QUATIFICATION OF TRANSCRIPT EXPRESSION WITH SALMON
# Created by: Tom Giles
# Date: 21st Dec 2017
# Version 0.1.1

# This script runs skewer salmon and samtools to generate very fast counts per transcript
# direct from FQ files. It assumes that the reads are formated *.R1.fastq.gz / *.R1.fastq.gz
# if your reads are formated differently please contact me or adjust the script below. 

##########################################################################################
#SET USER VARIABLES
##########################################################################################

TRANSCRIPTS_FASTA=$1
READ_FOLDER=$2

##########################################################################################
#RUN PIPELINE
##########################################################################################


# Build transcriptome index

if [ ! -d "${TRANSCRIPTS_FASTA%.fa}.index" ]; then
salmon index -t $TRANSCRIPTS_FASTA -i ${TRANSCRIPTS_FASTA%.fa}.index --type quasi -k 31
fi
# Process Fastq files

for fq1 in $(find $READ_FOLDER -name "*.R1.fastq.gz");

	do
	# set local variables
	fq2=${fq1%.R1.fastq.gz}.R2.fastq.gz
	fqt1=${fq1%.gz}-trimmed-pair1.fastq
	fqt2=${fq1%.gz}-trimmed-pair2.fastq
	fqt1c=${fq1%.R1.fastq.gz}_R1.trimmed.fastq
	fqt2c=${fq1%.R1.fastq.gz}_R2.trimmed.fastq
	salmon_qc=${fq1%.R1.fastq.gz}.salmon.quality
	salmon_qu=${fq1%.R1.fastq.gz}.salmon.quantification
	salmon_sam=${fq1%.R1.fastq.gz}.salmon.sam
	salmon_bam=${fq1%.R1.fastq.gz}.salmon.bam
	salmon_log=${fq1%.R1.fastq.gz}.salmon.log
	salmon_count=${fq1%.R1.fastq.gz}.salmon.count

	#trim file with skewer
	echo "trimming with skewer"
	skewer -t 16 -Q 20 -q 3 $fq1 $fq2; 
	mv $fqt1 $fqt1c; 
	mv $fqt2 $fqt2c;

	#Quantify with salmon
	echo "Quantifying with salmon"
	salmon quant -p 16 -i ${TRANSCRIPTS_FASTA%.fa}.index -l ISR --writeMappings -1 $fqt1c -2 $fqt2c -o $salmon_qu | samtools view -@4 -Sb - | samtools sort -@4 - > $salmon_bam

	#Count Alignments Per Sequence
	echo "Counting with Samtools"
	samtools index $salmon_bam
	samtools idxstats $salmon_bam | cut -f 1,3 > "$salmon_count"
done
