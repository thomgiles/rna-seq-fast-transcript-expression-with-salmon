# FAST TRIMMING, ALIGNMENTS AND QUATIFICATION OF TRANSCRIPT EXPRESSION WITH SALMON
# Created by: Tom Giles

# This script runs skewer salmon and samtools to generate very fast counts per transcript
# direct from FQ files. It assumes that the reads are formated *.R1.fastq.gz / *.R1.fastq.gz
# if your reads are formated differently please contact me or adjust the script below. 
